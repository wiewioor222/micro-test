package pk.TEST.hello.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pk.TEST.common.ContextUtils;

@Configuration
class HelloServiceConfiguration {
    private static final String TEST = "test";
    private static final String PK = "PK";

    @Autowired
    private ContextUtils contextUtils;

    @Bean
    HelloService helloService() {
        if(contextUtils.hasContext(TEST)) {
            return new HelloServiceTest();
        } else if (contextUtils.hasContext(PK)) {
            return new HelloServicePk();
        }

        return new DefaultHelloService();
    }

}
