package pk.TEST.hello;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import pk.TEST.hello.service.HelloService;
import pk.TEST.hello.service.TestException;

@RestController
@RequestMapping("/api/test")
class TestResource {

    @Autowired
    private HelloService helloService;

    @GetMapping
    String test() {
        return helloService.sayHello();
    }

    @GetMapping(value = "/error")
    void error() {
        throw new TestException();
    }

    @PostMapping
    ResponseEntity create(@Validated(Add.class) @RequestBody TestValidationDTO validationDTO) {
       return ResponseEntity.ok().build();
    }

    @PutMapping
    ResponseEntity update(@Validated(Mod.class) @RequestBody TestValidationDTO validationDTO) {
        return ResponseEntity.ok().build();
    }
}
