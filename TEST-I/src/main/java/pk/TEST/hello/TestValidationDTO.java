package pk.TEST.hello;

import pk.TEST.validation.MaxLength;

import javax.validation.constraints.NotNull;

public class TestValidationDTO {
    @NotNull(groups = Mod.class)
    private Long id;
    @MaxLength(value = 10, groups = {Add.class, Mod.class})
    private String name;

    public TestValidationDTO(){}

    public TestValidationDTO(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
