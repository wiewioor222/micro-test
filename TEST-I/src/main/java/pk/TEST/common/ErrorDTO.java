package pk.TEST.common;

import com.google.common.collect.Lists;

import java.io.Serializable;
import java.util.List;

public class ErrorDTO implements Serializable {

    private static final long serialVersionUID = -4404370563134765547L;

    private String message;
    private List<ErrorFieldDTO> errorFields = Lists.newArrayList();

    public ErrorDTO(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ErrorFieldDTO> getErrorFields() {
        return errorFields;
    }

    public void setErrorFields(List<ErrorFieldDTO> errorFields) {
        this.errorFields = errorFields;
    }
}
