package pk.TEST.common;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties( prefix = "app", ignoreInvalidFields = true)
public class AppProperties {
    private String contexts;

    public String getContexts() {
        return contexts;
    }

    public void setContexts(String contexts) {
        this.contexts = contexts;
    }
}
