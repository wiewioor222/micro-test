package pk.TEST.common;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import pk.TEST.hello.service.TestException;

import java.util.stream.Collectors;

@Slf4j
@RestControllerAdvice
public class AdviceController {

    @ExceptionHandler(TestException.class)
    @ResponseStatus(HttpStatus.I_AM_A_TEAPOT)
    ErrorDTO testException(TestException exception) {
        log.debug("",exception);
        return new ErrorDTO("Upsss");
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    ErrorDTO methodArgumentNotValidException(MethodArgumentNotValidException e) {
        ErrorDTO errorDTO = new ErrorDTO("Validation not pass");
        errorDTO.setErrorFields(e.getFieldErrors().stream().map(this::toFieldErrorDTO).collect(Collectors.toList()));
        return errorDTO;
    }

    private ErrorFieldDTO toFieldErrorDTO(FieldError fieldError) {
        return new ErrorFieldDTO(fieldError.getField(), fieldError.getDefaultMessage());
    }
}
