package pk.TEST.common;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.StringUtils.containsIgnoreCase;
import static org.apache.commons.lang3.StringUtils.split;

@Component
public class ContextUtils {

    @Autowired
    private AppProperties appProperties;

    public boolean hasContext(String context) {
        return getContexts().stream().anyMatch( v -> containsIgnoreCase(context, v));
    }

    public List<String> getContexts(){
        return Arrays.stream(split(appProperties.getContexts(),",")).map(StringUtils::trim).collect(toList());
    }
}
