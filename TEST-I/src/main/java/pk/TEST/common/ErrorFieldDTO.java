package pk.TEST.common;

import java.io.Serializable;

public class ErrorFieldDTO implements Serializable {

    private static final long serialVersionUID = -4404370563134765547L;

    private String name;
    private String message;

    public ErrorFieldDTO(String name, String message) {
        this.name = name;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
