package pk.TEST.test;

import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;

@Table(name = "TEST_SUB")
@Entity
@Audited
@AuditTable("TEST_SUB_H")
public class TestSubEntity {

    @Id
    private Long id;
    private String name;

    @ManyToOne
    @JoinColumn(name = "TEST_ID")
    private TestEntity test;
}
