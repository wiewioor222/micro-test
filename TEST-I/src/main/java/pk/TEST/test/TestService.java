package pk.TEST.test;

import org.hibernate.envers.AuditReader;
import org.springframework.beans.factory.annotation.Autowired;
import pk.TEST.audit.AuditServiceIml;
import pk.TEST.common.stereotype.OwnService;
import pk.TEST.mail.MailClient;
import pk.TEST.mail.MailResponseDTO;

@OwnService
public class TestService extends AuditServiceIml<TestEntity, Long> {

    private final AuditReader auditReader;
    private final MailClient mailClient;

    @Autowired
    public TestService(AuditReader auditReader, MailClient mailClient) {
        super(auditReader);
        this.auditReader = auditReader;
        this.mailClient = mailClient;
    }

    @Override
    protected Class<TestEntity> getResultClass() {
        return TestEntity.class;
    }

    public MailResponseDTO send(TestEntity testEntity) {
       return mailClient.send("test@wp.pl", "test2@wp.pl", testEntity.getName());
    }
}
