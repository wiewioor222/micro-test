package pk.TEST.test;

import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Table(name = "TEST")
@Entity
@Audited
@AuditTable("TEST_H")
public class TestEntity {
    @Id
    private Long id;

    private String name;

    @AuditJoinTable(name = "TEST_SUB_H")
    @OneToMany(mappedBy = "test", cascade = {CascadeType.MERGE, CascadeType.REMOVE, CascadeType.PERSIST}, orphanRemoval = true)
    private List<TestSubEntity> subEntities = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<TestSubEntity> getSubEntities() {
        return subEntities;
    }

    public void setSubEntities(List<TestSubEntity> subEntities) {
        this.subEntities = subEntities;
    }
}
