package pk.TEST;

import org.apache.commons.lang.StringUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.util.SocketUtils;
import pk.TEST.common.AppProperties;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@EnableConfigurationProperties({AppProperties.class})
@SpringBootApplication
public class TestApplication {

	private static final String INSTANCE_ID = "info.instanceId";
	private static final String SERVER_PORT = "server.port";
	private static final String MANAGEMENT_SERVER_PORT = "management.server.port";
	private static final String EUREKA_INSTANCE_METADATA_MAP_MANAGEMENT_PORT = "eureka.instance.metadata-map.management.port";
	private static final String DASH = "-";

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(TestApplication.class);
		app.setDefaultProperties(defaultProperties());
		app.run(args);
	}

	private static Map<String, Object> defaultProperties() {
		Map<String, Object> props = new HashMap<>();

		String instanceId = UUID.randomUUID().toString().replaceAll(DASH, StringUtils.EMPTY);
		props.put(INSTANCE_ID, instanceId);

		int serverPort = SocketUtils.findAvailableTcpPort();
		props.put(SERVER_PORT, serverPort);

		int managementPort = SocketUtils.findAvailableTcpPort();
		props.put(MANAGEMENT_SERVER_PORT, managementPort);
		props.put(EUREKA_INSTANCE_METADATA_MAP_MANAGEMENT_PORT, managementPort);

		return props;
	}

	@Bean
	MessageSource messageSource() {

		ReloadableResourceBundleMessageSource bundleMessageSource =  new ReloadableResourceBundleMessageSource();
		bundleMessageSource.setBasename("classpath:messages");
		bundleMessageSource.setDefaultEncoding("UTF-8");

		return bundleMessageSource;
	}
}
