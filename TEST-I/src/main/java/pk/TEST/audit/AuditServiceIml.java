package pk.TEST.audit;

import org.hibernate.envers.AuditReader;
import org.hibernate.envers.DefaultRevisionEntity;
import org.hibernate.envers.query.AuditEntity;
import pk.TEST.test.TestEntity;

import java.time.ZonedDateTime;
import java.util.Comparator;
import java.util.Date;

public abstract class AuditServiceIml<T, ID> {

    private AuditReader auditReader;
    
    public AuditServiceIml(AuditReader auditReader) {
        this.auditReader = auditReader;
    }

    public T find(ID id) {
        return auditReader.find(getResultClass(), id, new Date());
    }

    public T findByIdAndBeforeOrEqualsDate(ID id, ZonedDateTime date) {
        return auditReader.find(getResultClass(), id, Date.from(date.toInstant()));
    }

    public TestEntity findMaxByIdAndBeforeDate(Long id, ZonedDateTime date) {
        java.util.Date from = java.sql.Date.from(date.toInstant());

        return (TestEntity) auditReader.createQuery().forRevisionsOfEntity(TestEntity.class, false, true)
                .add(AuditEntity.id().eq(id)).addProjection(AuditEntity.id().distinct())
                .getResultList().stream().filter( o -> ((DefaultRevisionEntity)((Object[])o)[1]).getRevisionDate().before(from))
                .max(Comparator.comparing(o -> ((DefaultRevisionEntity)((Object[])o)[1]).getRevisionDate())).map(o -> ((Object[])o)[0]).orElse(null);
    }

    protected abstract Class<T> getResultClass();

    // <createSequence sequenceName="hibernate_sequence" startValue="1000" incrementBy="1"/>
}
