package pk.TEST.mail;

import org.springframework.http.HttpStatus;

public class MailResponseDTO {
    private HttpStatus httpStatus;
    private String value;

    public MailResponseDTO(HttpStatus httpStatus, String value) {
        this.httpStatus = httpStatus;
        this.value = value;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
