package pk.TEST.mail;

public interface MailClient {
    MailResponseDTO send(String to, String from, String message);
}
