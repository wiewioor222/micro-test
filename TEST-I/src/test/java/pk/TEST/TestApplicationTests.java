package pk.TEST;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.envers.AuditReader;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import pk.TEST.mail.MailClient;
import pk.TEST.mail.MailResponseDTO;
import pk.TEST.test.TestEntity;
import pk.TEST.test.TestService;

import static org.mockito.Mockito.*;

@RunWith(BlockJUnit4ClassRunner.class)
@SpringBootTest
@Slf4j
public class TestApplicationTests {

	@Mock
	private TestService testService;

	@Before
	public void init() {
		log.debug("init");
		MockitoAnnotations.openMocks(this);

		MailClient mailClient = mock(MailClient.class);

		when(mailClient.send(anyString(),anyString(),Mockito.isNull())).thenThrow(new NullPointerException());
		when(mailClient.send(anyString(),anyString(),anyString())).thenReturn(new MailResponseDTO(HttpStatus.OK,"all ok!!!"));

		testService = new TestService(mock(AuditReader.class), mailClient);
	}

	@Test(expected = NullPointerException.class)
	public void sendEmptyMessage() {
		testService.send(new TestEntity());
	}

	@Test
	public void sendNotEmptyMessage() {
		TestEntity testEntity = new TestEntity();
		testEntity.setName("XXXX");
		MailResponseDTO send = testService.send(testEntity);
		Assert.assertEquals(HttpStatus.OK, send.getHttpStatus());
	}
}
