package pk.TEST;

import org.hibernate.envers.AuditReader;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Configuration
class AppConfiguration {

    @PersistenceContext
    private EntityManager entityManager;

    @Bean
    AuditReader auditReader() {
        return Mockito.mock(AuditReader.class);
    }
}