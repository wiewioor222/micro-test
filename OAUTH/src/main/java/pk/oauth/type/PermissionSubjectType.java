package pk.oauth.type;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

public enum PermissionSubjectType {
	SCHEDULING(PermissionActionType.CREATE,  PermissionActionType.READ, PermissionActionType.DELETE),
	PLANNING(PermissionActionType.CREATE,  PermissionActionType.READ, PermissionActionType.DELETE),
	BIDDING(PermissionActionType.CREATE,  PermissionActionType.READ, PermissionActionType.DELETE),
	METERING(PermissionActionType.CREATE,  PermissionActionType.READ, PermissionActionType.DELETE),
	ADMINISTRATING;

	private List<PermissionActionType> actions;

	PermissionSubjectType(PermissionActionType... actions) {
		this.actions = newArrayList(actions);
	}

	public List<PermissionActionType> getActions() {
		return actions;
	}

}
