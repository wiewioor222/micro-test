package pk.oauth.type;

public enum PermissionActionType {
	CREATE,
	READ,
	DELETE
}
