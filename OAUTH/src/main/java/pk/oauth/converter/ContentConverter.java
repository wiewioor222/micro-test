package pk.oauth.converter;

import pk.oauth.exception.ConvertException;
import pk.oauth.model.IEntity;

import java.io.Serializable;

public interface ContentConverter<entityT extends IEntity<ID>, ID extends Serializable, T> {
	entityT convert(T t) throws ConvertException;
	T convert(entityT entity) throws ConvertException;
}
