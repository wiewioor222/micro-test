package pk.oauth.util;

import org.w3c.dom.Node;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.util.StreamReaderDelegate;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

public abstract class JaxbHelper {

	public static void marshall(Object obj, OutputStream outputStream, String xmlns) throws Exception {
		JAXBContext ctx = JAXBContext.newInstance(obj.getClass());
		Marshaller marshaller = ctx.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		marshaller.setProperty(Marshaller.JAXB_FRAGMENT, false);
		marshaller.marshal(new JAXBElement(new QName(xmlns, obj.getClass().getSimpleName()), obj.getClass(), obj),
				outputStream);
	}

	public static void marshall(Object obj, OutputStream outputStream, String namespaceURI, String localPart) throws Exception {
		JAXBContext ctx = JAXBContext.newInstance(obj.getClass());
		Marshaller marshaller = ctx.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		marshaller.setProperty(Marshaller.JAXB_FRAGMENT, false);
		marshaller.marshal(new JAXBElement(new QName(namespaceURI, localPart), obj.getClass(), obj),
				outputStream);
	}

	public static void marshall(OutputStream outputStream, JAXBElement element) throws Exception {
		JAXBContext ctx = JAXBContext.newInstance(element.getDeclaredType());
		Marshaller marshaller = ctx.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		marshaller.setProperty(Marshaller.JAXB_FRAGMENT, false);
		marshaller.marshal(element, outputStream);
	}

	public static String marshall(Object obj, String xmlns) throws Exception {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		JaxbHelper.marshall(obj, os, xmlns);
		return os.toString();
	}

	public static byte[] marshallToBytes(Object obj, String xmlns) throws Exception {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		JaxbHelper.marshall(obj, os, xmlns);
		return os.toByteArray();
	}

	public static byte[] marshallToBytes(Object obj, String namespaceURI, String localPart) throws Exception {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		JAXBContext ctx = JAXBContext.newInstance(obj.getClass());
		Marshaller marshaller = ctx.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		marshaller.setProperty(Marshaller.JAXB_FRAGMENT, false);
		marshaller.marshal(new JAXBElement(new QName(namespaceURI, localPart), obj.getClass(), obj),
				outputStream);

		return outputStream.toByteArray();
	}
	
	public static String marshall(Object obj) throws Exception {
		return JaxbHelper.marshall(obj, null);
	}

	public static <T> T unmarshal(byte[] content, Class<T> clazz) throws Exception {
		return unmarshal(new ByteArrayInputStream(content),clazz);
	}

	public static <T> T unmarshal(InputStream inputStream, Class<T> clazz) throws Exception {
		JAXBContext ctx = JAXBContext.newInstance(clazz);
		Unmarshaller unmarshaller = ctx.createUnmarshaller();
		Source source = new StreamSource(inputStream);
		return unmarshaller.unmarshal(source, clazz).getValue();
	}

	public static <T> T unmarshal(Source source, Class<T> clazz) throws Exception {
		JAXBContext ctx = JAXBContext.newInstance(clazz);
		Unmarshaller unmarshaller = ctx.createUnmarshaller();
		return unmarshaller.unmarshal(source, clazz).getValue();
	}

	public static <T> T unmarshal(String xml, Class<T> clazz) throws Exception {
		InputStream is = new ByteArrayInputStream(xml.getBytes());
		return JaxbHelper.unmarshal(is, clazz);
	}

	public static <T> T unmarshal(Node node, Class<T> clazz) throws Exception {
		JAXBContext ctx = JAXBContext.newInstance(clazz);
		Unmarshaller unmarshaller = ctx.createUnmarshaller();
		return unmarshaller.unmarshal(node, clazz).getValue();
	}

	public static <T> T unmarshalLowerCase(String xml, Class<T> clazz) throws Exception {
		InputStream is = new ByteArrayInputStream(xml.getBytes());
		return JaxbHelper.unmarshalLowerCase(is, clazz);
	}

	public static <T> T unmarshalLowerCase(InputStream inputStream, Class<T> clazz) throws Exception {
		JAXBContext jc = JAXBContext.newInstance(clazz);

		XMLInputFactory xif = XMLInputFactory.newInstance();
		XMLStreamReader xsr = new XMLReaderLowerCase(xif.createXMLStreamReader(inputStream));
		Unmarshaller unmarshaller = jc.createUnmarshaller();

		return (T) unmarshaller.unmarshal(xsr);
	}

	public static <T> T unmarshalIgnoreNamespace(String xml, Class<T> clazz) throws Exception {
		InputStream is = new ByteArrayInputStream(xml.getBytes());
		return JaxbHelper.unmarshalIgnoreNamespace(is, clazz);
	}

	public static <T> T unmarshalIgnoreNamespace(InputStream inputStream, Class<T> clazz) throws Exception {
		JAXBContext jc = JAXBContext.newInstance(clazz);

		XMLInputFactory xif = XMLInputFactory.newInstance();
		XMLStreamReader xsr = new XMLReaderWithoutNamespace(xif.createXMLStreamReader(inputStream));

		Unmarshaller unmarshaller = jc.createUnmarshaller();

		return (T) unmarshaller.unmarshal(xsr);
	}

	public static <T> T cast(Object obj, Class<T> clazz) throws Exception {
		String xml = marshall(obj);
		return unmarshal(xml, clazz);
	}

	private static class XMLReaderLowerCase extends StreamReaderDelegate {
		public XMLReaderLowerCase(XMLStreamReader reader) {
			super(reader);
		}

		@Override
		public String getAttributeLocalName(int index) {
			return super.getAttributeLocalName(index).toLowerCase().intern();
		}

		@Override
		public String getLocalName() {
			return super.getLocalName().toLowerCase().intern();
		}
	}

	private static class XMLReaderWithoutNamespace extends StreamReaderDelegate {
		public XMLReaderWithoutNamespace(XMLStreamReader reader) {
			super(reader);
		}

		@Override
		public String getAttributeNamespace(int arg0) {
			return "";
		}

		@Override
		public String getNamespaceURI() {
			return "";
		}
	}

}
