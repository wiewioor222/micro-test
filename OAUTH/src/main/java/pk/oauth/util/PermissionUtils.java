package pk.oauth.util;

import pk.oauth.type.PermissionActionType;
import pk.oauth.type.PermissionSubjectType;

public class PermissionUtils {

	public static final String ROLE_PREFIX = "ROLE_";

	public static String createRoleName(PermissionSubjectType subject, PermissionActionType action) {
		return createPermissionSubject(subject) + "_"+ action.name();
	}

	public static String createRoleName(PermissionSubjectType subject) {
		return createPermissionSubject(subject);
	}

	public static String createPermissionSubject(PermissionSubjectType subject) {
		return ROLE_PREFIX + subject.name();
	}
}
