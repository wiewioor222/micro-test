package pk.oauth.payload;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class SignUpRequest {

    @NotBlank
    @Size(min = 4, max = 40)
    private String login;

    @NotBlank
    @Size(min = 6, max = 20)
    private String password;

    public SignUpRequest(){}

    public SignUpRequest(@NotBlank @Size(min = 4, max = 40) String login, @NotBlank @Size(min = 6, max = 20) String password) {
        this.login = login;
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
