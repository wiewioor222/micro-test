package pk.oauth.payload;

import javax.validation.constraints.NotBlank;

public class LoginRequest {
    @NotBlank
    private String login;

    @NotBlank
    private String password;

    private String loginMethod;

    public LoginRequest(){}

    public LoginRequest(@NotBlank String login, @NotBlank String password, String loginMethod) {
        this.login = login;
        this.password = password;
        this.loginMethod = loginMethod;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLoginMethod() {
        return loginMethod;
    }

    public void setLoginMethod(String loginMethod) {
        this.loginMethod = loginMethod;
    }
}
