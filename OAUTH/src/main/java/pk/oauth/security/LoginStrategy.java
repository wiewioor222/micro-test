package pk.oauth.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

public interface LoginStrategy {
    Authentication authenticate(Authentication var1) throws AuthenticationException;
    String getStrategyName();
}
