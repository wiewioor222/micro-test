package pk.oauth.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import pk.oauth.config.UserService;
import pk.oauth.payload.JwtAuthenticationResponse;
import pk.oauth.payload.LoginRequest;
import pk.oauth.config.UserServiceInMemory;

@Component
public class AuthenticationContext {

    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private JwtTokenProvider tokenProvider;
    @Autowired
    private UserService userService;

    @Value("${app.jwtExpirationInMs}")
    private int jwtExpirationInMs;


    public Authentication authenticate(LoginRequest loginRequest) throws Exception {
        return this.authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getLogin(), loginRequest.getPassword()));
    }

    public JwtAuthenticationResponse getAuthenticationResponse(LoginRequest loginRequest) throws Exception {

        Authentication authentication =
                this.authenticate(loginRequest);

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwtToken = tokenProvider.generateToken(authentication);
        return new JwtAuthenticationResponse(jwtToken);
    }



}
