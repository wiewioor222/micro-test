package pk.oauth.security;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.collect.Lists;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;

@XmlRootElement(name = "user")
public class UserPrincipal implements UserDetails {
    private Long id;

    private String login;

    @JsonIgnore
    private String password;

    @JsonIgnore
    private Collection<GrantedAuthority> authorities;

    public UserPrincipal(){}

    public UserPrincipal(Long id, String login, String password, Collection<GrantedAuthority> authorities) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.authorities = authorities;
    }

    @Override
    @JsonIgnore
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    @JsonIgnore
    public String getPassword() {
        return password;
    }

    @XmlElement(name = "authorities")
    public Collection<String> getSimpleAuthorities() {

        if(CollectionUtils.isEmpty(authorities)){
            return Lists.newArrayList();
        }

        return authorities.stream().map(m -> m.getAuthority()).collect(Collectors.toList());
    }

    public void setSimpleAuthorities(Collection<String> authorities) {
        setAuthorities(authorities.stream().map(permission -> new SimpleGrantedAuthority(permission)).collect(Collectors.toList()));
    }

    @Override
    public String getUsername() {
        return login;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserPrincipal that = (UserPrincipal) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setAuthorities(Collection<GrantedAuthority> authorities) {
        this.authorities = authorities;
    }
}
