package pk.oauth.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import pk.oauth.commons.TokenUtils;
import pk.oauth.config.UserService;
import pk.oauth.payload.ApiResponse;
import pk.oauth.payload.JwtAuthenticationResponse;
import pk.oauth.payload.LoginRequest;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/auth")
public class AuthController {

	@Autowired
	private AuthenticationManager authenticationManager;
	@Autowired
	private UserService userService;
	@Autowired
	private JwtTokenProvider tokenProvider;
	@Value("${app.jwtExpirationInMs}")
	private int jwtExpirationInMs;

	@Autowired
	private AuthenticationContext authenticationContext;

	@PostMapping(value = "/login")
	public ResponseEntity<JwtAuthenticationResponse> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) throws Exception {

		if (!userService.existsByLogin(loginRequest.getLogin())) {
			return new ResponseEntity(new ApiResponse(false, "User not found"), HttpStatus.NOT_FOUND);
		}

		return ResponseEntity.ok(authenticationContext.getAuthenticationResponse(loginRequest));
	}

	@PreAuthorize("isAuthenticated()")
	@PostMapping(value = "/checkValid")
	public ResponseEntity<Boolean> checkValid(@RequestHeader("Authorization") String token) {
		String jwt = TokenUtils.extractToken(token);
		return ResponseEntity.ok(tokenProvider.validateToken(jwt));
	}

//	@PreAuthorize("isAuthenticated()")
//	@PostMapping(value = "/logout")
//	public ResponseEntity<?> deauthenticateUser(@RequestHeader("Authorization") String token) {
//
//		String jwt = TokenUtils.extractToken(token);
//		tokenValidityRepository.revoke(jwt);
//
//		return ResponseEntity.ok().build();
//	}

	@PreAuthorize("isAuthenticated()")
	@GetMapping(value = "/extractUserId")
	public ResponseEntity<Long> extractUserId(@RequestHeader("Authorization") String token) throws Exception {

		String jwt = TokenUtils.extractToken(token);
		return ResponseEntity.ok(tokenProvider.getUserIdFromJWT(jwt));
	}

	@PreAuthorize("isAuthenticated()")
	@GetMapping(value = "/extractUserData")
	public ResponseEntity<UserDetails> extractUserData(@RequestHeader("Authorization") String token) throws Exception {

		String jwt = TokenUtils.extractToken(token);
		return ResponseEntity.ok(tokenProvider.getUserDataFromJWT(jwt));
	}
}
