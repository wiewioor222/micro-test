package pk.oauth.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import pk.oauth.payload.ApiResponse;
import pk.oauth.payload.SignUpRequest;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("/api/auth")
public class SignUpController {

	private static final String DEFAULT_ROLE_NAME = "DEFAULT";

	@Autowired
	private PasswordEncoder passwordEncoder;

	@PostMapping(value = "/signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpRequest signUpRequest) {

//		if (userService.existsByLogin(signUpRequest.getLogin())) {
//			return new ResponseEntity(new ApiResponse(false, "Login is already taken!"), HttpStatus.BAD_REQUEST);
//		}

		URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/users/{username}").buildAndExpand(signUpRequest.getLogin()).toUri();

		return ResponseEntity.created(location).body(new ApiResponse(true, "User registered successfully"));
	}
}
