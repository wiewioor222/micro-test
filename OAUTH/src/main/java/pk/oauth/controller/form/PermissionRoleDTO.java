package pk.oauth.controller.form;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.security.core.GrantedAuthority;
import pk.oauth.dto.DTO;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PermissionRoleDTO implements Serializable, DTO<Long>, GrantedAuthority {
	private static final long serialVersionUID = 6807613261747702562L;
	private static final String ROLE = "ROLE_";

	private Long id;
	private String name;

	public PermissionRoleDTO(){}

	public PermissionRoleDTO(Long id, String name) {
		this.id = id;
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getAuthority() {
		return ROLE + name;
	}
}
