package pk.oauth.controller.form;

import com.google.common.collect.Lists;
import pk.oauth.type.PermissionSubjectType;

import java.io.Serializable;
import java.util.List;

public class PermissionGroupDTO implements Serializable {
	private static final long serialVersionUID = 6807613261747702561L;
	private PermissionSubjectType permissionSubject;
	private boolean checked;
	private List<PermissionActionDTO> actions = Lists.newArrayList();

	public PermissionGroupDTO(){}

	public PermissionGroupDTO(PermissionSubjectType permissionSubject, boolean checked, List<PermissionActionDTO> actions) {
		this.permissionSubject = permissionSubject;
		this.checked = checked;
		this.actions = actions;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public PermissionSubjectType getPermissionSubject() {
		return permissionSubject;
	}

	public void setPermissionSubject(PermissionSubjectType permissionSubject) {
		this.permissionSubject = permissionSubject;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public List<PermissionActionDTO> getActions() {
		return actions;
	}

	public void setActions(List<PermissionActionDTO> actions) {
		this.actions = actions;
	}
}
