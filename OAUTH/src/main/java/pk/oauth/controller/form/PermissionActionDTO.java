package pk.oauth.controller.form;

import pk.oauth.type.PermissionActionType;

import java.io.Serializable;

public class PermissionActionDTO implements Serializable {
	private static final long serialVersionUID = 1822823902145919042L;
	private PermissionActionType action;
	private Boolean checked;

	public PermissionActionDTO(){}

	public PermissionActionDTO(PermissionActionType action, Boolean checked) {
		this.action = action;
		this.checked = checked;
	}

	public PermissionActionType getAction() {
		return action;
	}

	public void setAction(PermissionActionType action) {
		this.action = action;
	}

	public Boolean getChecked() {
		return checked;
	}

	public void setChecked(Boolean checked) {
		this.checked = checked;
	}
}
