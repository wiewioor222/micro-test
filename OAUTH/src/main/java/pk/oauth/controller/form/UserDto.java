package pk.oauth.controller.form;

import com.google.common.collect.Sets;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import pk.oauth.dto.DTO;

import java.io.Serializable;
import java.util.Collection;
import java.util.Set;

public class UserDto implements DTO<Long>, Serializable, UserDetails {

	private static final long serialVersionUID = 3351944368641186373L;
	private Long id;
	private String login;
	private String password;
	private String email;
	private String firstName;
	private String lastName;
	private String phoneNumber;
	private Set<PermissionRoleDTO> permissions = Sets.newHashSet();

	public UserDto(){}

	public UserDto(Long id, String login, String password, String email, String firstName, String lastName, String phoneNumber, Set<PermissionRoleDTO> permissions) {
		this.id = id;
		this.login = login;
		this.password = password;
		this.email = email;
		this.firstName = firstName;
		this.lastName = lastName;
		this.phoneNumber = phoneNumber;
		this.permissions = permissions;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return permissions;
	}

	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return login;
	}

	@Override
	public boolean isAccountNonExpired() {
		return false;
	}

	@Override
	public boolean isAccountNonLocked() {
		return false;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return false;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Set<PermissionRoleDTO> getPermissions() {
		return permissions;
	}

	public void setPermissions(Set<PermissionRoleDTO> permissions) {
		this.permissions = permissions;
	}
}