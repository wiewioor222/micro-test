package pk.oauth.exception;

import org.apache.commons.collections4.CollectionUtils;
import pk.oauth.dto.InvalidDataDto;
import pk.oauth.dto.ValidationDataDto;

import java.util.List;

public class ValidationDataException extends RuntimeException {

    private List<ValidationDataDto> assignedInvalidData;

    public ValidationDataException() {
        super();
    }

    public ValidationDataException(String message) {
        super(message);
    }

    public ValidationDataException(String message, Throwable cause) {
        super(message, cause);
    }

    public ValidationDataException(List<ValidationDataDto> assignedInvalidData) {
        this.assignedInvalidData = assignedInvalidData;
    }

    protected ValidationDataException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

	public String getErrors() {
		StringBuilder sb = new StringBuilder();

		if (CollectionUtils.isNotEmpty(assignedInvalidData)) {

			for (ValidationDataDto validationDataDto : assignedInvalidData) {
				sb.append("[ name = " + validationDataDto.getName() + " ");

				sb.append('[');
				for (InvalidDataDto invalidDataDto : validationDataDto.getInvalidDate()) {
					sb.append(invalidDataDto.toString());
				}
				sb.append(']');
			}

			sb.append(']');
		}

		return sb.toString();
	}

    public ValidationDataException(Throwable cause) {
        super(cause);
    }

    public List<ValidationDataDto> getAssignedInvalidData() {
        return assignedInvalidData;
    }

    public void setAssignedInvalidData(List<ValidationDataDto> assignedInvalidData) {
        this.assignedInvalidData = assignedInvalidData;
    }
}
