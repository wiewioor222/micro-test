package pk.oauth.dto;

public interface DTO<T> {
	T getId();
}
