package pk.oauth.dto;

import java.util.Collection;
import java.util.Objects;

public class ValidationDataDto {
	private String name;
	private Long id;
	private Collection<InvalidDataDto> invalidDate;

	@Override
	public String toString() {
		return "ValidationDataDto{" +
				"name='" + name + '\'' +
				", id=" + id +
				", invalidDate=" + invalidDate +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		ValidationDataDto that = (ValidationDataDto) o;
		return Objects.equals(name, that.name) &&
				Objects.equals(id, that.id) &&
				Objects.equals(invalidDate, that.invalidDate);
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, id, invalidDate);
	}

	public ValidationDataDto(String name, Long id, Collection<InvalidDataDto> invalidDate) {
		this.name = name;
		this.id = id;
		this.invalidDate = invalidDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Collection<InvalidDataDto> getInvalidDate() {
		return invalidDate;
	}

	public void setInvalidDate(Collection<InvalidDataDto> invalidDate) {
		this.invalidDate = invalidDate;
	}
}
