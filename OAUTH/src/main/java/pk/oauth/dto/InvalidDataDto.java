package pk.oauth.dto;

import java.util.Objects;

public class InvalidDataDto {
    private String name;
    private String error;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public InvalidDataDto(String name, String error) {
        this.name = name;
        this.error = error;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InvalidDataDto that = (InvalidDataDto) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(error, that.error);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, error);
    }
}
