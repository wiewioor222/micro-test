package pk.oauth.config;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import pk.oauth.controller.form.PermissionRoleDTO;
import pk.oauth.controller.form.UserDto;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class UserServiceInMemory implements UserService {

    private List<UserDto> users = getUserDtos();

    private ArrayList<UserDto> getUserDtos() {
        UserDto userDto = new UserDto();
        userDto.setLogin("test@wp.pl");
        userDto.setEmail("test@wp.pl");
        userDto.setId(1l);
        userDto.setFirstName("Mat");
        userDto.setLastName("Pat");
        userDto.setPassword("test1");
        userDto.setPermissions(Sets.newHashSet(new PermissionRoleDTO(1l, "USER")));

        return Lists.newArrayList(userDto);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return users.stream().filter(userDto -> userDto.getLogin().equals(username)).findFirst().orElse(null);
    }

    public UserDetails loadUserById(Long userId) {
        return users.stream().filter(userDto -> userDto.getId().equals(userId)).findFirst().orElse(null);
    }

    public boolean existsByLogin(String login) {
        return users.stream().anyMatch(userDto -> userDto.getLogin().equalsIgnoreCase(login));
    }

    public Optional<UserDto> findByLogin(String usernameOrEmail) {
        return users.stream().filter(userDto -> userDto.getLogin().equalsIgnoreCase(usernameOrEmail)).findFirst();
    }

    public void save(UserDto userDto){
        users.add(userDto);
    }

    public void delete(Long id){
        users.removeIf(userDto -> userDto.getId().equals(id));
    }
}
