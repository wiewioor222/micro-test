package pk.oauth.config;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import pk.oauth.controller.form.UserDto;

import java.util.Optional;

public interface UserService extends UserDetailsService {
    UserDetails loadUserById(Long userId);
    boolean existsByLogin(String login);
    Optional<UserDto> findByLogin(String usernameOrEmail);
}
