package pk.oauth.commons;

import org.springframework.util.StringUtils;

public class TokenUtils {

    private static final String BEARER = "Bearer ";

    public static String extractToken(String bearerToken) {

        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith(BEARER)) {
            return bearerToken.substring(7);
        }

        return bearerToken;
    }
}
