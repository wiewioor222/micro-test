package pk.oauth.commons;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;

public class EnumSerializer extends StdSerializer<Enum<?>> {
	private static final long serialVersionUID = 3712574184534541622L;

	protected EnumSerializer() {
		super(Enum.class, false);
	}

	@Override
	public void serialize(Enum<?> value, JsonGenerator gen, SerializerProvider provider) throws IOException {
		gen.writeStartObject();
		gen.writeStringField("id", value.name());
		gen.writeStringField("name", value.name());
		gen.writeEndObject();
	}
}
