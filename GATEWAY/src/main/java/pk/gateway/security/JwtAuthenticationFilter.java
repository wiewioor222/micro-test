package pk.gateway.security;

import feign.FeignException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.Objects;

@Slf4j
public class JwtAuthenticationFilter implements WebFilter {

    private static final String AUTHORIZATION = "Authorization";

    @Autowired(required = false)
    private OauthClient oauthClient;

    @Autowired
    private Environment env;

    private Boolean checkValid(String jwt) {
        if(!Arrays.asList(env.getActiveProfiles()).contains("oauth")) {
            return true;
        }

        ResponseEntity<Boolean> responseEntity = oauthClient.checkValid(jwt);
        return Objects.nonNull(responseEntity) && responseEntity.getBody();
    }

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain filterChain) {
        ServerHttpResponse response = exchange.getResponse();
        ServerHttpRequest request = exchange.getRequest();
        try {
            String token = request.getHeaders().getFirst(AUTHORIZATION);

            if (StringUtils.hasText(token) && !checkValid(token)) {
                response.setStatusCode(HttpStatus.UNAUTHORIZED);
                return Mono.empty();
            } else {
                return filterChain.filter(exchange);
            }

        } catch (Exception ex) {
            log.error("Could not set user authentication in security context", ex);
            if(ex.getCause() instanceof FeignException){
                response.setStatusCode(HttpStatus.valueOf(((FeignException)ex.getCause()).status()));
                return Mono.error(ex);
            }

            throw ex;
        }
    }
}
