package pk.gateway.security;

import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Profile("oauth")
@Component
class OauthClientFallback implements OauthClient {
    @Override
    public ResponseEntity<Boolean> checkValid(String token) {
        return ResponseEntity.ok(false);
    }
}