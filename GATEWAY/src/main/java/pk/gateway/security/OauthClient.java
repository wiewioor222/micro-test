package pk.gateway.security;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;

@Profile("oauth")
@FeignClient(value = "oauth")
public interface OauthClient {

	@PostMapping(value = "/api/auth/checkValid")
	ResponseEntity<Boolean> checkValid(@RequestHeader("Authorization") String token);

}